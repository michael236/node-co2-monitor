# node-co2-monitor

Read CO₂ concentration, temperature and humidity from TFA Dostmann AirCO2NTROL Mini & Coach.

## Contents

* [Supported Hardware](#supported-hardware)
* [Install](#install)
* [Getting started](#getting-started)
* [API](#api)
    * [Methods](#methods)
    * [Events](#events)
* [Credits](#credits)
* [License](#license)


## Supported Hardware

* [TFA Dostmann AirCO2NTROL Mini - Monitor CO2 31.5006](https://www.tfa-dostmann.de/produkt/co2-monitor-airco2ntrol-mini-31-5006/)
* [TFA Dostmann AirCO2NTROL Coach - Monitor CO2 31.5009](https://www.tfa-dostmann.de/en/produkt/co2-monitor-airco2ntrol-coach-31-5009/)


## Install

```bash
npm install @jaller94/node-co2-monitor
```


## Getting started

```javascript
'use strict';
const CO2Monitor = require('@jaller94/node-co2-monitor');

const monitor = new CO2Monitor();

// Connect device.
monitor.connect((err) => {
    if (err) {
        return console.error(err.stack);
    }
    console.log('Monitor connected.');

    // Read data from CO2 monitor.
    monitor.transfer();
});

// Get results.
monitor.on('temp', (temperature) => {
    console.log(`temp: ${ temperature }`);
});
monitor.on('co2', (co2) => {
    console.log(`co2: ${ co2 }`);
});
monitor.on('hum', (humidity) => {
    console.log(`hum: ${ humidity }`);
});

// Error handler
monitor.on('error', (err) => {
    console.error(err.stack);
    // Disconnect device
    monitor.disconnect(() => {
        console.log('Monitor disconnected.');
        process.exit(0);
    });
});
```


## API
### Methods
#### new CO2Monitor(options) -> Object
Create CO2Monitor instance.

#### monitor.connect(Function callback)
Setup usb connection to CO2 monitor.

#### monitor.disconnect(Function callback)
Close device connection.

#### monitor.transfer([Function callback])
Start data transfer from CO2 monitor.

#### monitor.temperature -> Number
Get latest Ambient Temperature (Tamb) in ℃.

#### monitor.co2 -> Number
Get latest Relative Concentration of CO2 (CntR) in ppm.

#### monitor.humidity -> Number
Get latest Relative Humidity of CO2 in relative humidity * 100. Only the Coach supports this value. The Mini will report 0.

### Events

#### temp -> Number
Triggered by temperature update with Ambient Temperature (Tamb) in ℃.

#### co2 -> Number
Triggered by co2 update with Relative Concentration of CO2 (CntR) in ppm.

#### hum -> Number
Triggered by hum update with Relative Humidity of CO2 in relative humidity * 100. Only the Coach supports this value. The Mini will report 0.

#### error -> Error
Triggered by error.


## Projects using node-co2-monitor

* [CO2 Monitor Exporter](https://github.com/huhamhire/co2-monitor-exporter) - Prometheus exporter for CO2 concentration and indoor temperature from TFA Dostmann AirCO2NTROL Mini.


## Credits

Inspired by Henryk Plötz:
[Reverse-Engineering a low-cost USB CO₂ monitor](https://hackaday.io/project/5301-reverse-engineering-a-low-cost-usb-co-monitor/log/17909-all-your-base-are-belong-to-us).


## License

MIT
